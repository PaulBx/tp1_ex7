# Exerice 7 TP1

## Objectif
L'objectif de cet exercice est d'améliorer les classes de Test en incluant des log dedans permettant d'afficher explicitement les tests qui ont été effectué et qui ont réussit.

## Organisation
Même organisation que l'exercice précédent.

## Exécution du script
J'ai essayé d'installer une version de pytest plus récentes pour quelle soit compatible avec python3 afin que les logs puissent s'afficher lorsqu'on utilise seulement la commande pytest, seulement je n'ai pas réussit à trouver un moyen
de résoudre ce problème. Ainsi la commande `pytest` affichera seulement le nombre de tests effectué et le nombre de test réussit, pour voir s'afficher les logs, il faut appeler le script des classes de test directement :

    python3 Packages_Test/Calculator_test.py
    

## Résultats
Voici ce qu'on obtient avec l'éxecution des deux méthodes pour afficher les test (dans un premier temps Pytest, puis l'appel au script des classes):

![result](images/test.png)


## Ressources
exercice précédent :
https://gitlab.com/PaulBx/tp1_ex6.git