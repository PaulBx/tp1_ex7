#!/usr/bin/env python
# coding: utf-8
"""
author : paul Bouyssoux

"""

import unittest
import logging

from Package_Calculator.Calculator import SimpleCalculator


class TestSum(unittest.TestCase):
    """Cette classe permet de vérifier le bon focntionnement de la méthode
    fsum de la classe SimpleCalculator"""
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()
        self.log = logging.getLogger()

    def test_Sum2Int(self):
        """somme de deux entiers, le résultat doit être 7"""
        resultat = self.calculator.fsum(2, 5)
        self.assertEqual(resultat, 7)
        self.log.warning("test somme 2 entier OK")
        

    def test_SumIntStr(self):
        """Somme d'un entier et d'un caractère, le résultat 
        doit être ERROR"""
        resultat = self.calculator.fsum(2, "5")
        self.assertEqual(resultat, "ERROR")
        self.log.warning('test somme entier et str OK')
    
    def test_SumNegInt(self):
        """Somme entre un entier positif et un entier négatif
        le résultat doit être -3"""
        resultat = self.calculator.fsum(2, -5)
        self.assertEqual(resultat, -3)
        self.log.warning('somme entier positif et négatif OK')


class TestSub(unittest.TestCase):
    """Cette classe permet de vérifier le bon focntionnement de la méthode
    susbtract de la classe SimpleCalculator"""
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()
        self.log = logging.getLogger()

    def test_Sub2int(self):
        """Soustraction de deux entiers, le résultat doit être 3"""
        resultat = self.calculator.substract(5, 2)
        self.assertEqual(resultat, 3)
        self.log.warning('Différence deux entier OK')

    def test_SubIntStr(self):
        """Soustraction d'un entier et d'un caractère, le résultat 
        doit être ERROR"""
        resultat = self.calculator.substract(5, "2")
        self.assertEqual(resultat, "ERROR")
        self.log.warning('Différence entier et str OK')

    def test_SUmNegInt(self):
        """Soustraction entre un entier positif et un entier négatif
        le résultat doit être 7"""
        resultat = self.calculator.substract(2, -5)
        self.assertEqual(resultat, 7)
        self.log.warning('Différence entier positif et negatif OK')


class TestMultiply(unittest.TestCase):

    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()
        self.log = logging.getLogger()

    def test_Mult2int(self):
        """Multiplication entre deux entiers, le résultat doit être 10"""
        resultat = self.calculator.multiply(2, 5)
        self.assertEqual(resultat, 10)
        self.log.warning('Test multiplication deux entiers OK')

    def test_MultIntStr(self):
        """"Multiplication entre un entier et un caractère, le résultat
        doit être ERROR"""
        resultat = self.calculator.multiply(2, "5")
        self.assertEqual(resultat, "ERROR")
        self.log.warning('Test multiplication entier et str OK')

    def test_MultNegInt(self):
        """Multiplication entre deux entiers négatifs, le résultat doit 
        être 10"""
        resultat = self.calculator.multiply(-2, -5)
        self.assertEqual(resultat, 10)
        self.log.warning('Test multiplication deux entiers negatifs OK')

class TestDivide(unittest.TestCase):

    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()
        self.log = logging.getLogger()

    def test_Div2int(self):
        """Division entre deux entier, on doit obtenir 2"""
        resultat = self.calculator.divide(10, 5)
        self.assertEqual(resultat, 2)
        self.log.warning('Test division deux entier OK')

    def test_DivByZero(self):
        """Division par 0, nous devons obtenir un message d'erreur"""
        resultat = self.calculator.divide(10, 0)
        self.assertEqual(resultat, "vous avez essayé de diviser par zéro")
        self.log.warning('Test division par 0 OK')

    def test_DivNegInt(self):
        """DIvision d'un entier négatif par un positif, on doit obtenir -2"""
        resultat = self.calculator.divide(10, -5)
        self.assertEqual(resultat, -2)
        self.log.warning('Test division entier positif et negatif OK')

    def test_DivIntStr(self):
        """Division d'un entier par un caractère, on doit obtenir ERROR"""
        resultat = self.calculator.divide(10, "5")
        self.assertEqual(resultat, "ERROR")
        self.log.warning('Test division entier et str OK')




if __name__ == '__main__':
    unittest.main()





























