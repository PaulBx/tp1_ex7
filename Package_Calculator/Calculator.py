#!/usr/bin/env python
# coding: utf-8
"""
author : paul bouyssoux

-Ce script permet de créer une classe contenant les fonctions
 pour créer un calculateur simple (+,-,*,/).

"""

# Definition de la classe
class SimpleCalculator:
    """simple classe model, SimpleCalculator class"""

    def __init__(self):
        """constructeur de la classe, dans ce cas on initialise aucun attribut"""

    # Définition de la méthode somme
    def fsum(self, int_a, int_b):
        """Fonction permettant d'effectuer la somme des paramètres de la méthode seulement si
        les deux paramètres sont des entiers

        Parameters :
        a (int) : premier paramètre de la méthode

        b (int) : deuxième paramètre de la méthode


        returns :
        int : somme de a et b
        str : message erreur si un paramètre est non entier
        """""
        if (isinstance(int_a, int)) & (isinstance(int_b, int)):
            return int_a + int_b
        return "ERROR"


    # Définition de la méthode soustraction
    def substract(self, int_a, int_b):
        """Fonction permettant d'effectuer la somme des paramètres de la méthode seulement si
        les deux paramètres sont des entiers

        Parameters :
        a (int) : premier paramètre de la méthode

        b (int) : deuxième paramètre de la méthode


        returns :
        int : soustraction de a et b
        str : message erreur si un paramètre est non entier
        """""
        if (isinstance(int_a, int)) & (isinstance(int_b, int)):
            return int_a - int_b
        return "ERROR"

    # Définition de la méthode multiplier
    def multiply(self, int_a, int_b):
        """Fonction permettant d'effectuer la somme des paramètres de la méthode seulement si
        les deux paramètres sont des entiers

        Parameters :
        a (int) : premier paramètre de la méthode

        b (int) : deuxième paramètre de la méthode


        returns :
        int : multiplication de a par b
        str : message erreur si un paramètre est non entier
        """""
        if (isinstance(int_a, int)) & (isinstance(int_b, int)):
            return int_a * int_b
        return "ERROR"

    # D&finition de la méthode diviser
    def divide(self, int_a, int_b):
        """Fonction permettant d'effectuer la somme des paramètres de la méthode seulement si
        les deux paramètres sont des entiers et que le deuxième est non nul

        Parameters :
        a (int) : premier paramètre de la méthode

        b (int) : deuxième paramètre de la méthode


        returns :
        int : division de a par b
        str : message erreur si un paramètre est non entier ou que le deuxième est nul
        """""
        if (isinstance(int_a, int)) & (isinstance(int_b, int)):
            if int_b != 0:
                return int_a / int_b
            return "vous avez essayé de diviser par zéro"
        return "ERROR"
